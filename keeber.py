from datetime import datetime, time
import json
from pynput.keyboard import Controller
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep


url = "https://mekanisk.co/collections/fjell/products/fjell-r5-limited-edition-keyboard-kit"
test_url = "https://mekanisk.co/collections/keyboard-parts/products/wt60-d"

keyboard = Controller()

def wait_start(run_time):
    start_time = time(*(map(int, run_time.split(':'))))
    while start_time > datetime.today().time():
        sleep(.1)
    return True

def get_config():
    """
    Reads the configuration data from the config.json file.
    """
    file = open('config.json', 'r')
    json_data = json.load(file)
    file.close()
    return json_data

def get_browser():
    """
    Returns back a browser instance of Google Chrome.
    """
    return webdriver.Chrome("F:\\Downloads\\chromedriver")

def login(browser, login_info):
    """
    Navigates to and logs user in.
    """
    browser.get("https://mekanisk.co/account/login?return_url=%2Faccount")
    username = browser.find_element_by_id("CustomerEmail")
    username.send_keys(login_info['username'])

    password = browser.find_element_by_id("CustomerPassword")
    password.send_keys(login_info['password'])

    sign_in = browser.find_element_by_css_selector("input[type='Submit']")
    sign_in.click()

def fill_shipping(browser, ship_info):
    """
    Fills out shipping information in the payment screen.
    """

    address = browser.find_element_by_id("checkout_shipping_address_address1")
    address.send_keys(ship_info['address'])

    city = browser.find_element_by_id("checkout_shipping_address_city")
    city.send_keys(ship_info['city'])

    zip = browser.find_element_by_id("checkout_shipping_address_zip")
    zip.send_keys(ship_info['zip'])

    phone = browser.find_element_by_id("checkout_shipping_address_phone")
    phone.send_keys(ship_info['phone'])

def confirm(browser):
    browser.find_element_by_id("continue_button").click()

def fill_payment(browser, payment_info):
    """
    Fills out credit card information in the payment screen.
    """
    cc = browser.find_element_by_xpath(
        "//div[@data-card-fields='number']").click()
    keyboard.type(payment_info['credit_card'])

    sleep(0.2)

    name = browser.find_element_by_xpath(
        "//div[@data-card-fields='name']").click()
    keyboard.type(payment_info['name'])

    sleep(0.2)

    expiration = browser.find_element_by_xpath(
        "//div[@data-card-fields='expiry']").click()
    keyboard.type("09")
    sleep(0.2)
    keyboard.type("2023")

    sleep(0.2)

    cvv = browser.find_element_by_xpath(
        "//div[@data-card-fields='verification_value']").click()
    keyboard.type(payment_info['cvv'])

if __name__ == "__main__":
    wait_start('21:59:40')
    json_data = get_config()
    ship_info = json_data['shipping_info']
    login_info = json_data['login_info']
    payment_info = json_data['cc_info']
    browser = get_browser()
    login(browser, login_info)
    browser.get(url)
    # TODO Loop on datetime for 10 PM EST
    # Set implicit wait here to .5 second for potential perfromance issues.
    browser.implicitly_wait(.5)
    while True:
        try:
            add_to_cart = browser.find_element_by_name("add")
            add_to_cart.click()
            checkout = browser.find_element_by_name("checkout")
            break
        except:
            browser.refresh()
            continue

    # Increase wait here for loading of shipping page (shopify)
    browser.implicitly_wait(6)
    checkout.click()
    fill_shipping(browser, ship_info)
    confirm(browser)
    confirm(browser)
    fill_payment(browser, payment_info)
    confirm(browser)
    confirm(browser)

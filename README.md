# KEEBER

### Configuration File Structure:
```
{
  "cc_info": {
    "credit_card": "",
    "cvv": "",
    "exp_date": "",
    "type": "",
    "name": ""
  },
  "shipping_info": {
    "address": "",
    "zip": "",
    "city": "",
    "state": "",
    "country": "",
    "phone": ""
  },
  "login_info": {
    "username": "",
    "password": ""
  }
}
```
